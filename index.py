import webbrowser
from flask import Flask, render_template, request
from flask_socketio import SocketIO


webbrowser.open ("http://127.0.0.1:5000")

app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
def index ():
    code = """
    <html>
        <head>
            <title> Enter your name </title>
        </head>
        <body>
            <form action="/chat" method="post">
                <input name="name" type="text" placeholder="Enter your name">
                <button type="submit"> Submit </button>
            </form>
        </body>
    </html>
    """
    return code

@app.route('/chat', methods=['GET', 'POST'])
def chat ():
    name = ''
    if request.method == 'POST':
        name = request.form.get('name')
        return render_template("index.html", name=name)
    else:
        name = "Nothing"
        return render_template("index.html", name=name)

def result ():
	print ("Receive message")

@socketio.on("my event")
def handle_custom_event (json):
	print ("Json : "+str(json))
	socketio.emit("my response", json, callback=result)


if __name__ == '__main__':
	socketio.run(app)